//  Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

// liliupeng@inspur.com


#pragma once

#include "abstract_void_ref.h"
#include "pure_mem/abstract_version_node.h"
#include <atomic>
#include <list>

namespace rocksdb {
// TreeVoidRef object pointer is stored as tree leaf node.
// tree just index user key, no HLC or sequence number.
// TreeVoidRef object contains multi version values for one user key.
class TreeVoidRef2 : public ITreeVoidRef {
public:
  TreeVoidRef2() : ITreeVoidRef(ITreeVoidRef::RefType::REFTYPE2) {
    version_head_.store(nullptr);
    next_.store(nullptr);
    prev_.store(nullptr);
  }
  ~TreeVoidRef2() override {
    VersionNode *cur = version_head_.load(), *next = nullptr;
    while (cur != nullptr) {
      next = cur->Next();
      delete cur;
      cur = next;
    }
  }

  ITreeVoidRef *Next() override { return next_.load(); }
  ITreeVoidRef *Prev() override { return prev_.load(); }

  VersionNode *getContentList() override { return this->version_head_.load(); }

  bool CASContentList(VersionNode *expected, VersionNode *x) override {
    return version_head_.compare_exchange_strong(expected, x);
  }
  void setContentList(VersionNode *x) override { return version_head_.store(x); }

  void setNext(ITreeVoidRef *x) override { return next_.store(x); }

  void setPrev(ITreeVoidRef *x) override { return prev_.store(x); }

  bool CASNext(ITreeVoidRef *expected, ITreeVoidRef *x) override {
    return next_.compare_exchange_strong(expected, x);
  }

  bool CASPrev(ITreeVoidRef *expected, ITreeVoidRef *x) override {
    return prev_.compare_exchange_strong(expected, x);
  }


private:
  // double link list, store multi version values of current key.
  std::atomic<VersionNode *> version_head_;
  // double link list of TreeVoidRef, each TreeVoidRef node need store prev and
  // next pointer.
  std::atomic<ITreeVoidRef *> prev_;
  std::atomic<ITreeVoidRef *> next_;
};
} // namespace rocksdb
