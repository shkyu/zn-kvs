// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#pragma once

#include "pure_mem/key_index/abstract_tree.h"
#include "pure_mem/key_index/art/syn_art_fullkey/abstract_loadkey.h"
#include "pure_mem/key_index/art/syn_art_fullkey/art_tree.h"

namespace rocksdb {
/**
 * 基于　ＡＲＴ树　和　ＲＯＷＥＸ并发控制机制，实现的　ITree　索引类．
 * 该类使用ｒｏｗｅｘ的逻辑，需要实现对应的接口
 * 
 */
class RowexTreeFullKey : public ITree, public syn_art_fullkey::ILoadKey {

public:
  RowexTreeFullKey(LoadKeyFromValue load, Logger *logger);
  ~RowexTreeFullKey() override;
  bool insertNoReplace(const Slice &key, void *curNode,
                       void *&retNode) override;
  void *getGT(const Slice &key) override;
  void *getGE(const Slice &key) override;

  void parseTid2Key(void* tid, Slice &key);

private:
  syn_art_fullkey::Tree *tree_;
};

} // namespace rocksdb